# Unit Test for the usage of the KFParticle package within ALICE

### Test

Macro to generate MC Events containing either:  <br />
D0 -> K- + pi+ or K0S -> pi- + pi+ or lambda0 -> p+ + pi- or their charge conjugates <br />
inside the barrel part of the ALICE detector (45 < theta < 135) <br />
It is possible to generate only the jets or the whole event, whereas only jets are faster <br />
 
How to use the test: <br />
The files run.C, parametrization_ideal_noEL.root and DrawKFTestResults.C are needed. <br />
To run the test execute: aliroot -l run.C <br />
UnitTest.C generates trees with the resolutions and pulls of the reconstruction <br />
DrawTestResults.C plots the results and generates a root file with the histograms and graphs <br />

### Parametrization
The UnitTest.C macro uses a parametrization to smear the parameter that is created py fitting the covariance matrix of real data.
The macro cCreateParam.C can be used to generate new distributions given a filtered tree (Marian).

### IMPORTANT
The test was developed for the 2015 version of KFParticle that at this point was not included in AliROOT.
By now it is possible to load KFParticle directly in AliROOT. The test has to be updated accordinly



